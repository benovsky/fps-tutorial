﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTexture : MonoBehaviour
{
    public Texture m_MainTexture;
    Renderer m_Renderer;
   
    public void ChangedToCracked()
    {
        m_Renderer = GetComponent<Renderer>();
        m_Renderer.material.SetTexture("_MainTex", m_MainTexture);
    }
}
