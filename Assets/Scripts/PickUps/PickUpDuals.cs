﻿using UnityEngine;
public class PickUpDuals : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null)
        {
            GameObject weaponHolster = GameObject.Find("WeaponHolster");
            WeaponSwitch weaponSwitch = weaponHolster.GetComponent<WeaponSwitch>();
            //UNIQUE
            int weaponId = (int)Helpers.Weapons.Duals;            
            GameObject weaponParent = weaponHolster.transform.Find("Duals").gameObject;
            Shoot_Duals weaponScript = weaponParent.GetComponentInChildren<Shoot_Duals>();
            //END-UNIQUE
            WeaponAvailable weaponAvailable = weaponParent.GetComponent<WeaponAvailable>();            
            if (weaponAvailable.available)
            {
                int ammo = weaponScript.maxAmmo * 3;
                bool updateUi = weaponSwitch.selectedWeapon == weaponId;
                weaponScript.GainAmmo(ammo, updateUi);
            }
            else
            {
                GameManager.weaponInfo[weaponId].Available = true;
                GameManager.weaponInfo[weaponId].CurrentAmmo = weaponScript.maxAmmo;
                GameManager.weaponInfo[weaponId].RemainingAmmo = weaponScript.maxAmmo * 2;
                weaponParent.SetActive(true);
                weaponAvailable.AddWeapon();
                weaponSwitch.ChangeWeapon(weaponId);
            }
        }
    }
}
