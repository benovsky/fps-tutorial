﻿using UnityEngine;

public class PickUpShotgun : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null)
        {
            GameObject weaponHolster = GameObject.Find("WeaponHolster");
            WeaponSwitch weaponSwitch = weaponHolster.GetComponent<WeaponSwitch>();
            GameObject shotgunParent = weaponHolster.transform.Find("Shotgun").gameObject;
            Shoot_Shotgun shotgunWeapon = shotgunParent.GetComponentInChildren<Shoot_Shotgun>();
            WeaponAvailable weaponAvailable = shotgunParent.GetComponent<WeaponAvailable>();
            if (weaponAvailable.available)
            {
                int ammo = shotgunWeapon.maxAmmo * 3;
                bool updateUi = weaponSwitch.selectedWeapon == (int)Helpers.Weapons.Shotgun;
                shotgunWeapon.GainAmmo(ammo, updateUi);
            }
            else
            {
                GameManager.weaponInfo[(int)Helpers.Weapons.Shotgun].Available = true;
                GameManager.weaponInfo[(int)Helpers.Weapons.Shotgun].CurrentAmmo = shotgunWeapon.maxAmmo;
                GameManager.weaponInfo[(int)Helpers.Weapons.Shotgun].RemainingAmmo = shotgunWeapon.maxAmmo * 2;
                shotgunParent.SetActive(true);
                weaponAvailable.AddWeapon();
                weaponSwitch.ChangeWeapon((int)Helpers.Weapons.Shotgun);
            }
        }
    }
}