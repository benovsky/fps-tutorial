﻿using UnityEngine;

public class PickUpBehaviour : MonoBehaviour
{
    public GameObject pickUpParent;
    public GameObject pointLight;
    public GameObject idleParticles;
    public GameObject pickedParticles;
    public AudioSource healthSound;
    public GameObject rotatingModel;
    public bool validatePickup = false;

    private void OnTriggerEnter(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null && !validatePickup)
        {
            Debug.Log("Picked By: " + other.name);
            PickedUp();
        }
    }

    public void PickedUp()
    {
        healthSound.Play();
        gameObject.SetActive(false);
        pickedParticles.SetActive(true);
        idleParticles.SetActive(false);
        rotatingModel.SetActive(false);
        pointLight.SetActive(false);
        Destroy(pickUpParent, 2);
    }
}
