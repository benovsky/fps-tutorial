﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    public float healthGain = 500f;
    public PickUpBehaviour triggerArea;
    private void OnTriggerEnter(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null && !player.FullHealth)
        {
            Debug.Log(other.name + " gains " + healthGain + " health!");
            player.GainHealth(healthGain);
            triggerArea.PickedUp();
        }
    }
}
