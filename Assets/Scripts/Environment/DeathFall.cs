﻿using UnityEngine;

public class DeathFall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null)
        {
            player.Die();
        }
    }
}
