﻿using UnityEngine;

public class Shatter : MonoBehaviour
{
    public GameObject destroyedVersion;
    public GameObject itemDrop;
    private bool shatterable = true;

    public void ShatterTarget()
    {
        if (shatterable)
        {
            shatterable = false;
            GameObject shatteredGO = Instantiate(destroyedVersion, transform.position, transform.rotation);
            if (itemDrop != null)
            {
                Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1.2f, transform.position.z);
                Instantiate(itemDrop, pos, transform.rotation);
            }
            Destroy(gameObject);
            Destroy(shatteredGO, 7f);
        }
    }
}
