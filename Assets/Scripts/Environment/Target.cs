﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public float health = 50;
    public HealthBar healthBar;
    public bool showHealth = false;
    public Animator animator;
    public TargetTexture texture;

    private float currentHP;
    private bool _damaged = false;
    private void Start()
    {
        currentHP = health;
        if (showHealth)
        {
            healthBar.SetMaxHealth(currentHP);
        }
    }

    public void TakeDamage(float amount)
    {
        Debug.Log(currentHP + " - " + amount);
        currentHP -= amount;
        if (texture != null && !_damaged)
            texture.ChangedToCracked();
        if (showHealth)
        {
            healthBar.SetHealth(currentHP);
            animator.SetTrigger("TargetHit");
        }
        if (currentHP <= 0)
        {
            Shatter shatter = gameObject.GetComponent<Shatter>();
            if (shatter)
            {
                shatter.ShatterTarget();
            }
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }

}
