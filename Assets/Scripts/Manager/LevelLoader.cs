﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;

    public Animator death;
    public float deathTime = 5f;

    public GameObject loadingScreen;
    public Slider progressSlider;
    public Text progressText;

    public void LoadNextLevel()
    {
        //StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
        LoadLevelAsync(SceneManager.GetActiveScene().buildIndex + 1);
    }
    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }

    public void PlayerDeath()
    {
        StartCoroutine(LoadAfterDeath(SceneManager.GetActiveScene().buildIndex));
    }

    IEnumerator LoadAfterDeath(int levelIndex)
    {
        Debug.Log("Dang, dead");
        Time.timeScale = 0;
        death.SetTrigger("Died");
        yield return new WaitForSecondsRealtime(deathTime);
        Time.timeScale = 1;
        //LoadLevelAsync(levelIndex);
        SceneManager.LoadScene(levelIndex);
    }

    //LOADING METHODS
    public void LoadLevelAsync(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            Debug.Log(progress);
            progressSlider.value = progress;
            progressText.text = (System.Math.Truncate(progress) * 100f) + "%";
            yield return null;
        }
    }
}
