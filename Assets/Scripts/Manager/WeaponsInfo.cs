﻿public class WeaponsInfo
{
    public int Id { get; set; }
    public int MaxAmmo { get; set; }
    public int RemainingAmmo { get; set; }
    public int CurrentAmmo { get; set; }
    public bool Available { get; set; }
}
