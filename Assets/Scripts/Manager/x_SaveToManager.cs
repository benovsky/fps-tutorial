﻿using UnityEngine;

public class SaveToManager : MonoBehaviour
{
    public Shoot_Pistol pistol;
    public WeaponAvailable pistolBool;
    public Shoot_Duals duals;
    public WeaponAvailable dualsBool;
    public Shoot_Shotgun shotgun;
    public WeaponAvailable shotgunBool;
    public Shoot_Pistol rifle;
    public WeaponAvailable rifleBool;
    public Shoot_Sniper sniper;
    public WeaponAvailable sniperBool;
    //public Shoot_Duck duck;
    //public WeaponAvailable duckBool;
    void Weapons()
    {
        //Ids
        int pistolId = (int)Helpers.Weapons.Pistol;
        int dualsId = (int)Helpers.Weapons.Duals;
        int shotgunId = (int)Helpers.Weapons.Shotgun;
        int rifleId = (int)Helpers.Weapons.Rifle;
        int sniperId = (int)Helpers.Weapons.Sniper;
        
        //Available Weapons
        GameManager.weaponInfo[pistolId].Available = pistolBool.available;
        GameManager.weaponInfo[dualsId].Available = dualsBool.available;
        GameManager.weaponInfo[shotgunId].Available = shotgunBool.available;
        GameManager.weaponInfo[rifleId].Available = rifleBool.available;
        GameManager.weaponInfo[sniperId].Available = sniperBool.available;
        
        //Max Ammo
        GameManager.weaponInfo[pistolId].MaxAmmo = pistol.maxAmmo;
        GameManager.weaponInfo[dualsId].MaxAmmo = duals.maxAmmo;
        GameManager.weaponInfo[shotgunId].MaxAmmo = shotgun.maxAmmo;
        GameManager.weaponInfo[rifleId].MaxAmmo = rifle.maxAmmo;
        GameManager.weaponInfo[sniperId].MaxAmmo = sniper.maxAmmo;
        
        //Remaining Ammo
        //GameManager.weaponInfo[pistolId].RemainingAmmo = pistol.remainingAmmo - pistol.currentAmmo;
        //GameManager.weaponInfo[dualsId].RemainingAmmo = duals.remainingAmmo - duals.currentAmmo;
        GameManager.weaponInfo[shotgunId].RemainingAmmo = shotgun.remainingAmmo - shotgun.currentAmmo;
        //GameManager.weaponInfo[rifleId].RemainingAmmo = rifle.remainingAmmo - rifle.currentAmmo;
        //GameManager.weaponInfo[sniperId].RemainingAmmo = sniper.remainingAmmo - sniper.currentAmmo;

    }
}
