﻿public static class GameManager
{
    public static float maxHP;
    public static WeaponsInfo[] weaponInfo = InitializeArray<WeaponsInfo>(8);
    public static WeaponsInfo[] auxWeaponInfo = InitializeArray<WeaponsInfo>(8);
    public static int selectedWeapon;
    public static int auxSelectedWeapon;

    public static void SetWeaponInfo()
    {
        for (int i = 0; i < weaponInfo.Length; i++)
        {
            WeaponsInfo prev = weaponInfo[i];
            WeaponsInfo next = auxWeaponInfo[i];
            prev.Available = next.Available;
            prev.CurrentAmmo = next.CurrentAmmo;
            prev.Id = next.Id;
            prev.MaxAmmo = next.MaxAmmo;
            prev.RemainingAmmo = next.RemainingAmmo;            
        }
        selectedWeapon = auxSelectedWeapon;

    }
    public static void SetAuxiliaryInfo()
    {
        for (int i = 0; i < weaponInfo.Length; i++)
        {
            WeaponsInfo prev = auxWeaponInfo[i];
            WeaponsInfo next = weaponInfo[i];
            prev.Available = next.Available;
            prev.CurrentAmmo = next.CurrentAmmo;
            prev.Id = next.Id;
            prev.MaxAmmo = next.MaxAmmo;
            prev.RemainingAmmo = next.RemainingAmmo;
        }
        auxSelectedWeapon = selectedWeapon;
    }
    private static T[] InitializeArray<T>(int length) where T : new()
    {
        T[] array = new T[length];
        for (int i = 0; i < length; ++i)
        {
            array[i] = new T();
        }

        return array;
    }
}
