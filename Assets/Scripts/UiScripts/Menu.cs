﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject credits;
    public GameObject mainMenu;
    public GameObject alterMenu;
    public float transitionTime = 2f;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "GameOver")
        {
            Debug.Log("Game over loaded.");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void StartGame()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        alterMenu.SetActive(true);
        mainMenu.SetActive(false);
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void CreditsOpen()
    {
        credits.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void CreditsClose()
    {
        credits.SetActive(false);
        mainMenu.SetActive(true);
    }

    #region ExitMethod
    public void Exit()
    {
        Debug.Log("Quit");
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
    }

    #endregion
}
