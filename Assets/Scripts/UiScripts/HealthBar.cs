﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public Slider damageSlider;
    public Gradient gradient;
    public Image fill;
    public GameObject healthBarText;
    public Text textHP;
    public Text textMaxHP;
    public bool showNumbers;
    public bool showDamageFill;

    private bool _enterCR = true;
    private bool _isHurt = false;
    private float _lastHealth;
    public void SetMaxHealth(float health)
    {
        slider.maxValue = slider.value = health;
        fill.color = gradient.Evaluate(1f);
        if (showNumbers)
        {
            healthBarText.SetActive(true);
            textHP.text = textMaxHP.text = health.ToString();
        }
        if (showDamageFill)
        {
            damageSlider.maxValue = damageSlider.value = health;
            _lastHealth = health;
        }

    }

    public void SetHealth(float health)
    {
        if (showDamageFill)
        {
            _lastHealth = health;
            if (health >= slider.value)
            {
                damageSlider.value = health;
            }
            else
            {
                _isHurt = true;
                if (_enterCR)
                {
                    StartCoroutine(DamageFill());
                }
            }
        }
        slider.value = health;
        fill.color = gradient.Evaluate(slider.normalizedValue);
        if (showNumbers)
        {
            textHP.text = health.ToString();
        }
    }

    public void GainMaxHealth(float health)
    {
        slider.maxValue = health;
        fill.color = gradient.Evaluate(slider.normalizedValue);
        if (showNumbers)
        {
            textHP.text = health.ToString();
        }
        if (showDamageFill)
        {
            damageSlider.maxValue = health;
        }
    }

    IEnumerator DamageFill()
    {
        _enterCR = false;
        do
        {
            _isHurt = false;
            yield return new WaitForSeconds(1f);
        } while (_isHurt);
        _enterCR = true;
        damageSlider.value = _lastHealth;
    }
}
