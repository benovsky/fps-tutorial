﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballBehaviour : MonoBehaviour
{
    public float speed = 10.0f;
    public float damage = 800;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0,0, speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("FIREBALL HITS" + other.name);
        PlayerStats player = other.GetComponent<PlayerStats>();
        if(player != null)
        {
            player.TakeDamage(damage);
        }
        //Don't destroy on collision with layers: 2-IgnoreRaycast, 10-EnemyAttack and 11-EnemyBody
        if(other.gameObject.layer == 2 || other.gameObject.layer == 10 || other.gameObject.layer == 11)
        {
            return;
        }
        Destroy(this.gameObject);
    }
}
