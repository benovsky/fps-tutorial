﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float spawnRadius = 2f;
    public float spawnInterval = 3f;
    public bool spawnerEnabled = false;
    public float spawnVerticalOffset = 1.5f;

    private float _nextTimeToSpawn = 0f;
    private bool _firstSpawn = true;
    void Update()
    {
        if (spawnerEnabled && Time.time >= _nextTimeToSpawn)
        {
            Vector3 localPosition = gameObject.transform.position;
            if (_firstSpawn)
                _firstSpawn = false;
            else
            {
                localPosition.x += Random.Range(-spawnRadius, spawnRadius);
                localPosition.z += Random.Range(-spawnRadius, spawnRadius);
            }
            localPosition.y += spawnVerticalOffset;
            Instantiate(enemyPrefab, localPosition, transform.rotation);
            _nextTimeToSpawn = Time.time + spawnInterval;
        }
    }
}
