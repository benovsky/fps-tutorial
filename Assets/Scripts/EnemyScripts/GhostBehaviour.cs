﻿using UnityEngine;

public class GhostBehaviour : MonoBehaviour
{
    public float speed = 3.0f;
    public float onTouchDamage = 10f;
    public float onTouchPerSecond = 10;

    private float _nextTimeToHurt = 0f;
   void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
    }
    void OnTriggerStay(Collider other)
    {
        PlayerStats player = other.GetComponent<PlayerStats>();
        if (player != null)
        {
            Debug.Log("Touching Player");
            if (Time.time >= _nextTimeToHurt)
            {
                Debug.Log("Damage OnTouch");
                _nextTimeToHurt = Time.time + 1f / onTouchPerSecond;
                player.TakeDamage(onTouchDamage);
            }
        }
    }
}
