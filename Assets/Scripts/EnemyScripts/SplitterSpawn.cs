﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitterSpawn : MonoBehaviour
{
    public float spawnOffset = 1f;

    public GameObject whiteGhostPrefab;
    public GameObject greenDasherPrefab;
    public GameObject violetExploderPrefab;
    public GameObject redShooterPrefab;

    private Vector3 _spawnArea1;
    private Vector3 _spawnArea2;
    private Vector3 _spawnArea3;
    private Vector3 _spawnArea4;
    void Start()
    {
        Vector3 here = transform.position;
        _spawnArea1 = new Vector3(here.x, here.y + spawnOffset, here.z + spawnOffset);
        _spawnArea2 = new Vector3(here.x, here.y + spawnOffset, here.z - spawnOffset);
        _spawnArea3 = new Vector3(here.x, here.y - spawnOffset, here.z + spawnOffset);
        _spawnArea4 = new Vector3(here.x, here.y - spawnOffset, here.z - spawnOffset);
        Instantiate(GetRandomSpawn(), _spawnArea1, transform.rotation);
        Instantiate(GetRandomSpawn(), _spawnArea2, transform.rotation);
        Instantiate(GetRandomSpawn(), _spawnArea3, transform.rotation);
        Instantiate(GetRandomSpawn(), _spawnArea4, transform.rotation);
        Destroy(gameObject, 3f);
    }

    private GameObject GetRandomSpawn() {
        int roll = Random.Range(1, 20);
        switch (roll)
        {
            case 1:
            case 2:
            case 3:
            case 4:
                return greenDasherPrefab;
            case 5:
            case 6:
            case 7:
            case 8:
                return redShooterPrefab;
            case 9:
            case 10:
            case 11:
            case 12:
                return violetExploderPrefab;
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                return whiteGhostPrefab;
            case 20:
                return new GameObject();
            default:
                return whiteGhostPrefab;
        }
    }
}
