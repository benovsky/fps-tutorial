﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherBehaviour : MonoBehaviour
{
    public GhostBehaviour baseBehaviour;
    public float dashSpeed = 15.0f;
    public float dashSpeedOffset = 2f;
    public float dashDuration = 2f;
    public float dashDurationOffset = .5f;
    public float walkDuration = 2f;
    public float walkDurationOffset = .5f;

    private float _nextTimeToDash = 0f;
    private float _baseSpeed;
    //private float current_speed;
    private bool dashing;
    void Start()
    {
        //current_speed = baseBehaviour.speed;
        _baseSpeed = baseBehaviour.speed;
        dashing = false;
    }
    void Update()
    {
        if (Time.time >= _nextTimeToDash)
            ChangeSpeed();
    }
    void ChangeSpeed()
    {
        if (dashing)
        {   //Stop Dashing
            baseBehaviour.speed = _baseSpeed;
            _nextTimeToDash = Time.time + walkDuration + Random.Range(-walkDurationOffset, walkDurationOffset);
        }
        else
        {   //Start Dashing
            baseBehaviour.speed = dashSpeed;
            _nextTimeToDash = Time.time + dashDuration + Random.Range(-dashDurationOffset, dashDurationOffset);
        }
        dashing = !dashing;
    }
}
