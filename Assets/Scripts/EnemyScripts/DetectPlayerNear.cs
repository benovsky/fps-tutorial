﻿using UnityEngine;

public class DetectPlayerNear : MonoBehaviour
{
    public EnemySpawner enemySpawner;
    public ParticleSystem spawnerFog;

    private void Start()
    {
        spawnerFog.Stop();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemySpawner.spawnerEnabled = true;
            spawnerFog.Play();
        }
    }
}
