﻿using UnityEngine;

public class GhostExplosion : MonoBehaviour
{
    public float explosionDamage = 800f;

    void Start()
    {
        Destroy(gameObject, 1f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerStats>() != null)
        {
            FindObjectOfType<PlayerStats>().TakeDamage(explosionDamage);
        }
        else if (other.GetComponent<Target>() != null)
        {
            other.GetComponent<Target>().TakeDamage(explosionDamage);
        }
    }
}
