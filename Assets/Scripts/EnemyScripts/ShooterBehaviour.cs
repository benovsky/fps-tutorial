﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterBehaviour : MonoBehaviour
{
    public GameObject fireballPrefab;
    public float ShootRate = 1f;
    public float atkDetectionRange = 100f;

    private float _nextTimeToShoot = 0f;
    void Update()
    {
        if (Time.time >= _nextTimeToShoot)
        {
            _nextTimeToShoot = Time.time + 1f / ShootRate;
            ShootFireBall();
        }
    }
    void ShootFireBall()
    {
        //Detect player through other enemy objects
        int layer1 = 10;                           //EnemyAttack Layer
        int layer2 = 11;                           //EnemyBody Layer
        int layermask1 = 1 << layer1;              //Bitshift
        int layermask2 = 1 << layer2;              //Bitshift
        int finalmask = ~layermask1 & ~layermask2; //(Bitwise Complement) All layers except these.

        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.SphereCast(ray, 0.75f, out RaycastHit hit, atkDetectionRange, finalmask))
        {
            GameObject hitObject = hit.transform.gameObject;
            if (hitObject.GetComponent<PlayerStats>())
            {
                Instantiate(fireballPrefab, transform.TransformPoint(0, 1f, 1.5f), transform.rotation);
            }
        }
    }
}
