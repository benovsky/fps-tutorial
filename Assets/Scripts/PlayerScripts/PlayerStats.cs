﻿using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float maxHP = 2500f;
    public HealthBar healthbar;
    public LevelLoader levelLoader;

    private float currentHP;
    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
        healthbar.SetMaxHealth(maxHP);
    }

    public bool FullHealth
    {
        get { return (currentHP >= maxHP); }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("HP:" + currentHP);
    }

    public void TakeDamage(float damage)
    {
        currentHP = currentHP = Mathf.Clamp(currentHP - damage, 0, maxHP);
        healthbar.SetHealth(currentHP);
        if (currentHP <= 0)
            Die();
    }

    public void GainHealth(float recovery)
    {
        currentHP = Mathf.Clamp(currentHP + recovery, 0, maxHP);
        healthbar.SetHealth(currentHP);
    }

    public void Die()
    {
        levelLoader.PlayerDeath();
    }
}
