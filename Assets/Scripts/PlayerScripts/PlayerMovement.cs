﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float sprint = 2f;
    public float gravity = -9.8f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    float finalSpeed;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }
    //Slope Movement
    //https://forum.unity.com/threads/character-movement-and-slopes.290381/
    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
                
        if (Input.GetButton("Run"))
        {
            finalSpeed = speed * sprint;
        }
        else
        {
            finalSpeed = speed;
        }

        controller.Move(move * finalSpeed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            source.Play();
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
            
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}
