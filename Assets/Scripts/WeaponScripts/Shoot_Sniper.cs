﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Shoot_Sniper : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    public float unscopedSpread = 15f;
    public float impactForce = 30f;
    public float scopedFOV = 16f;
    public float scopedFOV2 = 8f;
    //public int maxAmmo = 6;
    public float reloadTime = 1.2f;
    public int maxAmmo;
    public int currentAmmo;
    public int remainingAmmo;

    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;
    public Animator animator;
    public GameObject scopeOverlay;
    public GameObject weaponCamera;
    public Text uiAmmoCount;
    public Text uiMaxAmmo;

    private float nextTimeToFire = 0f;
    private AudioSource source;
    private int scopeLevel = 0;
    private float normalFOV;
    private float normalSensitivity;
    //private int currentAmmo;
    private bool isReloading = false;
    private int id;

    void Start()
    {
        id = (int)Helpers.Weapons.Sniper;
        //currentAmmo = maxAmmo;
        source = GetComponent<AudioSource>();
        currentAmmo = GameManager.weaponInfo[id].CurrentAmmo;
        remainingAmmo = GameManager.weaponInfo[id].RemainingAmmo;
        uiAmmoCount.text = currentAmmo.ToString();
        uiMaxAmmo.text = remainingAmmo.ToString();
        normalFOV = fpsCam.fieldOfView;
        normalSensitivity = fpsCam.GetComponent<MouseLook>().mouseSensitivity;
        //uiAmmoCount.text = uiMaxAmmo.text = maxAmmo.ToString();
    }

    private void OnEnable()
    {
        isReloading = false;
        animator.SetBool("IsReloading", false);
        scopeLevel = 0;
        uiMaxAmmo.text = remainingAmmo.ToString();
        uiAmmoCount.text = currentAmmo.ToString();
    }

    private void OnDisable()
    {
        scopeLevel = 0;
        OnUnscoped();
        animator.SetBool("IsReloading", false);
    }
    void Update()
    {

        GameManager.weaponInfo[id].RemainingAmmo = remainingAmmo;
        GameManager.weaponInfo[id].CurrentAmmo = currentAmmo;
        if (isReloading)
            return;
        if (currentAmmo <= 0 && remainingAmmo > 0)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }

        if (Input.GetButtonDown("Reload") && currentAmmo < maxAmmo && remainingAmmo > 0)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && currentAmmo > 0)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            scopeLevel = (scopeLevel == 2) ? 0 : ++scopeLevel;
            switch (scopeLevel)
            {
                case 1:
                    animator.SetBool("IsScoped", true);
                    StartCoroutine(OnScoped());
                    break;
                case 2:
                    fpsCam.fieldOfView = scopedFOV2;
                    fpsCam.GetComponent<MouseLook>().mouseSensitivity = normalSensitivity / 4;
                    break;
                default:
                    OnUnscoped();
                    break;
            }
        }
    }

    IEnumerator Reload()
    {
        scopeLevel = 0;
        OnUnscoped();
        isReloading = true;
        Debug.Log("Reloading");
        if (currentAmmo == 0)
            yield return new WaitForSeconds(.25f);
        animator.SetBool("IsReloading", true);
        yield return new WaitForSeconds(reloadTime - .25f);
        animator.SetBool("IsReloading", false);
        yield return new WaitForSeconds(.25f);
        GetAmmo();
        isReloading = false;
    }
    private void GetAmmo()
    {
        int neededAmmo = maxAmmo - currentAmmo;
        if (remainingAmmo >= neededAmmo)
        {
            remainingAmmo -= neededAmmo;
            currentAmmo = maxAmmo;
        }
        else
        {
            currentAmmo += remainingAmmo;
            remainingAmmo = 0;
        }
        uiAmmoCount.text = currentAmmo.ToString();
        uiMaxAmmo.text = remainingAmmo.ToString();
    }
    public void GainAmmo(int ammoGained, bool updateUI)
    {
        remainingAmmo += ammoGained;
        if (updateUI)
            uiMaxAmmo.text = remainingAmmo.ToString();
    }

    void Shoot()
    {
        source.Play();
        muzzleFlash.Play();

        currentAmmo--;
        uiAmmoCount.text = currentAmmo.ToString();

        Transform cam = fpsCam.transform;

        int layerMask = 1 << 2;     // "2:Ingnore Raycast" Layer.
        layerMask = ~layerMask;     //All but layer 2, bit shift.

        Vector3 newVector;
        if (scopeLevel == 0)
        {
            float bulletAngle = Random.Range(-unscopedSpread, unscopedSpread);
            int ran = Random.Range(-1, 1);
            int ran2 = Random.Range(-1, 1);
            int ran3 = Random.Range(-1, 1);
            Vector3 noAngle = fpsCam.transform.forward;
            Quaternion spreadAngle = Quaternion.AngleAxis(bulletAngle, new Vector3(ran2, ran, ran3));
            newVector = spreadAngle * noAngle;
        }
        else
        {
            newVector = cam.forward;
        }

        RaycastHit hit;
        if (Physics.Raycast(cam.position, newVector, out hit, range, layerMask))
        {
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
        }

    }

    void OnUnscoped()
    {
        animator.SetBool("IsScoped", false);
        fpsCam.GetComponent<MouseLook>().mouseSensitivity = normalSensitivity;
        scopeOverlay.SetActive(false);
        weaponCamera.SetActive(true);
        fpsCam.fieldOfView = normalFOV;
    }

    IEnumerator OnScoped()
    {
        yield return new WaitForSeconds(.15f);
        scopeOverlay.SetActive(true);
        weaponCamera.SetActive(false);
        fpsCam.fieldOfView = scopedFOV;
        fpsCam.GetComponent<MouseLook>().mouseSensitivity = normalSensitivity / 2;
    }
}
