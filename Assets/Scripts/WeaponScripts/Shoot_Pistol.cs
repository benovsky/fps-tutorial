﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Shoot_Pistol : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    public float impactForce = 30f;
    public float sightsFOV = 40f;
    public float sightsSpeed = .2f;
    public int maxAmmo = 12;
    public float reloadTime = 1.2f;

    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;
    public Animator animator;
    public Text uiAmmoCount;
    public Text uiMaxAmmo;
    public GameObject crosshair;

    private float nextTimeToFire = 0f;
    private AudioSource source;
    private int currentAmmo;
    private bool isReloading = false;
    private float normalFOV;

    void Start()
    {
        source = GetComponent<AudioSource>();
        crosshair.SetActive(true);
        currentAmmo = maxAmmo;
        uiAmmoCount.text = maxAmmo.ToString();
        //uiAmmoCount.text = uiMaxAmmo.text = maxAmmo.ToString();
        uiMaxAmmo.text = "∞";
        normalFOV = fpsCam.fieldOfView;
    }
    private void OnEnable()
    {
        crosshair.SetActive(true);
        isReloading = false;
        animator.SetBool("IsReloading", false);
        //uiMaxAmmo.text = maxAmmo.ToString();
        uiMaxAmmo.text = "∞";
        uiAmmoCount.text = currentAmmo.ToString();
    }
    private void OnDisable()
    {
        crosshair.SetActive(false);
        animator.SetBool("IsReloading", false);
        animator.SetBool("IsScoped", false);
        fpsCam.fieldOfView = normalFOV;
    }
    void Update()
    {
        if (isReloading)
        {
            animator.SetBool("IsScoped", false);
            ChangeSights(normalFOV);
            return;
        }
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }
        if (Input.GetButtonDown("Reload") && currentAmmo < maxAmmo)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            source.Play();
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }

        if (Input.GetButton("Fire2") && !isReloading)
        {
            animator.SetBool("IsScoped", true);
            ChangeSights(sightsFOV);
        }
        else
        {
            animator.SetBool("IsScoped", false);
            ChangeSights(normalFOV);
        }
    }
    void Shoot()
    {
        muzzleFlash.Play();
        currentAmmo--;
        uiAmmoCount.text = currentAmmo.ToString();

        int layerMask = 1 << 2; // "2:Ingnore Raycast" Layer.
        layerMask = ~layerMask; //All but layer 2, bit shift.

        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range, layerMask))
        {
            Debug.Log(hit.transform.name);
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
        }
    }
    IEnumerator Reload()
    {
        isReloading = true;
        Debug.Log("Reloading");
        if (currentAmmo == 0)
            yield return new WaitForSeconds(.25f);
        animator.SetBool("IsReloading", true);
        yield return new WaitForSeconds(reloadTime - .25f);
        animator.SetBool("IsReloading", false);
        yield return new WaitForSeconds(.25f);
        currentAmmo = maxAmmo;
        uiAmmoCount.text = currentAmmo.ToString();
        isReloading = false;
    }
    private void ChangeSights(float targetFOV)
    {
        fpsCam.fieldOfView = Mathf.Lerp(fpsCam.fieldOfView, targetFOV, sightsSpeed);
    }
}
