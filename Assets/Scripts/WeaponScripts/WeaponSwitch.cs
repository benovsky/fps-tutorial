﻿using UnityEngine;

public class WeaponSwitch : MonoBehaviour
{
    public int selectedWeapon = 0;
    void Start()
    {
        GameManager.SetWeaponInfo();
        selectedWeapon = GameManager.selectedWeapon;
        SelectWeapon();
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (GameManager.weaponInfo[i].Available)
            {
               weapon.gameObject.GetComponent<WeaponAvailable>().available = true;
            }
            i++;
        }
    }

    void Update()
    {
        int previousSelectedWeapon = selectedWeapon;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            do
            {
                SelectNextWeapon();
            } while (!transform.GetChild(selectedWeapon).GetComponent<WeaponAvailable>().available);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            do
            {
                SelectPrevWeapon();
            } while (!transform.GetChild(selectedWeapon).GetComponent<WeaponAvailable>().available);
        }

        /*if (Input.GetKey(KeyCode.Alpha1))
        {
            selectedWeapon = 0;
        }
        if (Input.GetKey(KeyCode.Alpha2) && transform.childCount >= 2)
        {
            selectedWeapon = 1;
        }*/

        if (previousSelectedWeapon != selectedWeapon)
        {
            SelectWeapon();
            GameManager.selectedWeapon = selectedWeapon;
        }
    }

    void SelectNextWeapon()
    {
        if (selectedWeapon >= transform.childCount - 1)
            selectedWeapon = 0;
        else
            selectedWeapon++;
    }

    void SelectPrevWeapon()
    {
        if (selectedWeapon <= 0)
            selectedWeapon = transform.childCount - 1;
        else
            selectedWeapon--;
    }
    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon)
                weapon.gameObject.SetActive(true);
            else
                weapon.gameObject.SetActive(false);
            i++;
        }
    }
    public void ChangeWeapon(int weapon)
    {
        GameManager.selectedWeapon = selectedWeapon = weapon;
        SelectWeapon();
    }
}
