﻿using UnityEngine;

public class WeaponAvailable : MonoBehaviour
{
    public GameObject weaponParent;
    public Helpers.Weapons weapon;
    public bool available;
    public void AddWeapon()
    {
        available = true;
    }
    public void RemoveWeapon()
    {
        available = false;
    }
}
