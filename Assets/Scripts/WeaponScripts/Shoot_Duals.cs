﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Shoot_Duals : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 100f;
    public float impactForce = 30f;
    public float sightsFOV = 40f;
    public float sightsSpeed = .2f;
    public float reloadTime = 1.2f;
    public int maxAmmo = 20;
    public int currentAmmo;
    public int remainingAmmo;

    public GameObject pistolOne;
    public GameObject pistolTwo;
    public ParticleSystem muzzleFlashOne;
    public ParticleSystem muzzleFlashTwo;
    public Animator animatorOne;
    public Animator animatorTwo;

    public Camera fpsCam;
    public GameObject impactEffect;
    public Text uiAmmoCount;
    public Text uiMaxAmmo;
    public GameObject crosshair;

    private float nextTimeToFire = 0f;
    private AudioSource source;
    //private int currentAmmo;
    private bool isReloading = false;
    private float normalFOV;
    private bool rightPistolFire;
    private int id;
    void Start()
    {
        id = (int)Helpers.Weapons.Duals;
        source = GetComponent<AudioSource>();
        crosshair.SetActive(true);
        //currentAmmo = maxAmmo;
        currentAmmo = GameManager.weaponInfo[id].CurrentAmmo;
        remainingAmmo = GameManager.weaponInfo[id].RemainingAmmo;
        //uiAmmoCount.text = uiMaxAmmo.text = maxAmmo.ToString();
        uiAmmoCount.text = currentAmmo.ToString();
        uiMaxAmmo.text = remainingAmmo.ToString();
        normalFOV = fpsCam.fieldOfView;
    }
    private void OnEnable()
    {
        crosshair.SetActive(true);
        isReloading = false;
        animatorOne.SetBool("IsReloading", false);
        animatorTwo.SetBool("IsReloading", false);
        //uiMaxAmmo.text = maxAmmo.ToString();
        uiMaxAmmo.text = remainingAmmo.ToString();
        uiAmmoCount.text = currentAmmo.ToString();
    }
    private void OnDisable()
    {
        crosshair.SetActive(false);
        animatorOne.SetBool("IsReloading", false);
        animatorTwo.SetBool("IsReloading", false);
        animatorOne.SetBool("IsScoped", false);
        animatorTwo.SetBool("IsScoped", false);
        fpsCam.fieldOfView = normalFOV;
    }
    void Update()
    {
        GameManager.weaponInfo[id].RemainingAmmo = remainingAmmo;
        GameManager.weaponInfo[id].CurrentAmmo = currentAmmo;
        if (isReloading)
        {
            animatorOne.SetBool("IsScoped", false);
            animatorTwo.SetBool("IsScoped", false);
            ChangeSights(normalFOV);
            return;
        }
        if (currentAmmo <= 0 && remainingAmmo > 0)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }
        if (Input.GetButtonDown("Reload") && currentAmmo < maxAmmo && remainingAmmo > 0)
        {
            StartCoroutine(Reload());
            return; //Stops Update
        }

        if (Input.GetButtonDown("Fire1") && currentAmmo > 0 && Time.time >= nextTimeToFire)
        {
            source.Play();
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }

        if (Input.GetButton("Fire2") && !isReloading)
        {
            animatorOne.SetBool("IsScoped", true);
            animatorTwo.SetBool("IsScoped", true);
            ChangeSights(sightsFOV);
        }
        else
        {
            animatorOne.SetBool("IsScoped", false);
            animatorTwo.SetBool("IsScoped", false);
            ChangeSights(normalFOV);
        }
    }
    void Shoot()
    {
        if (rightPistolFire)
        {
            muzzleFlashOne.Play();
        }
        else
        {
            muzzleFlashTwo.Play();
        }
        rightPistolFire = !rightPistolFire;
        currentAmmo--;
        uiAmmoCount.text = currentAmmo.ToString();

        int layerMask = 1 << 2; // "2:Ingnore Raycast" Layer.
        layerMask = ~layerMask; //All but layer 2, bit shift.

        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range, layerMask))
        {
            Debug.Log(hit.transform.name);
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
        }
    }
    IEnumerator Reload()
    {
        isReloading = true;
        Debug.Log("Reloading");
        if (currentAmmo == 0)
            yield return new WaitForSeconds(.25f);
        animatorOne.SetBool("IsReloading", true);
        animatorTwo.SetBool("IsReloading", true);
        yield return new WaitForSeconds(reloadTime - .25f);
        animatorOne.SetBool("IsReloading", false);
        animatorTwo.SetBool("IsReloading", false);
        yield return new WaitForSeconds(.25f);
        GetAmmo();
        isReloading = false;
    }
    private void GetAmmo()
    {
        int neededAmmo = maxAmmo - currentAmmo;
        if (remainingAmmo >= neededAmmo)
        {
            remainingAmmo -= neededAmmo;
            currentAmmo = maxAmmo;
        }
        else
        {
            currentAmmo += remainingAmmo;
            remainingAmmo = 0;
        }
        uiAmmoCount.text = currentAmmo.ToString();
        uiMaxAmmo.text = remainingAmmo.ToString();
    }
    public void GainAmmo(int ammoGained, bool updateUI)
    {
        remainingAmmo += ammoGained;
        if (updateUI)
            uiMaxAmmo.text = remainingAmmo.ToString();
    }
    private void ChangeSights(float targetFOV)
    {
        fpsCam.fieldOfView = Mathf.Lerp(fpsCam.fieldOfView, targetFOV, sightsSpeed);
    }
}
