﻿public class Helpers
{
    public enum Weapons
    {
        Pistol, Duals, Shotgun, Rifle, Sniper, Duck
    }

    public enum Damage
    {
        Chip = 1,
        Poor = 50,
        Light = 100,
        Minor = 150, 
        Moderate = 500,
        Severe = 1000,
        Critical = 1500,
        Fatal = 5000
    }    
}